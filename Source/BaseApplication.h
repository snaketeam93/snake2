#pragma once

namespace APPLICATION
{
	class BaseApplication
	{
	protected:
		std::vector<ApplicationStage*> Stages;
		ApplicationStage* ActiveStage;

		sf::RenderWindow AppWindow;
		float FPS;
	public:
		BaseApplication(void) : ActiveStage(NULL) {}
		virtual ~BaseApplication(void) {}

		void SetActiveStage(ApplicationStage* stage);
		void UpdateEvents();
		void Init(int Width, int Height, const std::string& title);
		void Run();
		void Release();

		virtual void OnUpdate(float dt) {}
		virtual void OnStageChange(ApplicationStage* prev, ApplicationStage* current) {}
		virtual void OnInit() {}
		virtual void OnRelease() {}
	};
}