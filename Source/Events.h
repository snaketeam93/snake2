#pragma once

namespace GAME
{
	class GameEvent
	{
	public:
		enum TYPE { NONE = 0, STARTGAME, CLIENTINDEX, CHANGEDIR, SYNCHOBJECTS, NEWOBJECT, CLOSEGAME, ONSCORE, ONBONUS, SNAKEDEATH, 
			STARTGAMEREQUEST, HIGHSCORESREQUEST, CLIENTNAME, CLOSECONNECTION, SCORESPACK, NUMEVENTS };

		static std::string TypeStr[NUMEVENTS];

		bool Valid() { return Type >= 0 && Type < NUMEVENTS; }

		const std::string& ToString() { return Valid() ? TypeStr[Type] : TypeStr[NONE]; }

		GameEvent() {}

		GameEvent(TYPE t) : Type(t) {}

		GameEvent(TYPE t, int p1) : Type(t)
		{
			if (t == GameEvent::CLIENTINDEX)
			{
				ClientIndex.ClientIndex= p1;
			}
			else if (t == GameEvent::SNAKEDEATH)
			{
				SnakeDeath.SnakeIndex = p1;
			}
			if (t == HIGHSCORESREQUEST)
			{
				HighScoresRequest.ClientIndex = p1;
			}
		}


		GameEvent(TYPE t, int p1, int p2) : Type(t)
		{
			
			if (t == ONSCORE)
			{
				OnScore.SnakeIndex = p1;
				OnScore.Score = p2;
			}
			else if (t == ONBONUS)
			{
				OnBonus.SnakeIndex = p1;
				OnBonus.BonusIndex = p2;
			}
			else if (t == SYNCHOBJECTS)
			{
				SyncObjects.Objects = p1;
				SyncObjects.SyncTime = p2;
				SyncObjects.Data = NULL;
			}
			else if (t == CLIENTNAME)
			{
				ClientName.ClientIndex = p1;
				ClientName.NameLength = p2;
				ClientName.pName = NULL;
			}
			else if (t == SCORESPACK)
			{
				ScoresPack.ClientIndex = p1;
				ScoresPack.Size = p2;
			}
		}

		GameEvent(TYPE t, int p1, int p2, int p3, std::string* names) : Type(t)
		{
			if (t == STARTGAME)
			{
				StartGame.NumPlayers = p1;
				StartGame.MaxObjects = p2;
				StartGame.Time = p3;
				StartGame.Names = names;
			}
		}

		GameEvent(TYPE t, int p1, short px, short py, short dx, short dy) : Type(t)
		{
			if (t == CHANGEDIR)
			{
				ChangeDir.SnakeIndex = p1;
				ChangeDir.PosX = px;
				ChangeDir.PosY = py;
				ChangeDir.DirX = dx;
				ChangeDir.DirY = dy;
			}
		}

		GameEvent(TYPE t, int p1, int p2, int p3) : Type(t)
		{
			if (t == NEWOBJECT)
			{
				NewObject.ObjectIndex = p1;
				NewObject.Generator1 = p2;
				NewObject.Generator2 = p3;
			}
		}

		TYPE Type;
		union
		{
			struct { int ClientIndex; int NameLength; std::string* pName; }					ClientName;	
			struct { int SnakeIndex; }														SnakeDeath;	
			struct { short NumPlayers; short MaxObjects; int Time; std::string* Names; }	StartGame;	
			struct { int ClientIndex; }														ClientIndex;
			struct { int SnakeIndex; short PosX; short PosY; short DirX; short DirY; }		ChangeDir;	
			struct { int Objects; float* Data; int SyncTime; }								SyncObjects;	
			struct { int ObjectIndex; int Generator1; int Generator2; }						NewObject;	
			struct { int SnakeIndex; int Score; }											OnScore;		
			struct { int SnakeIndex; int BonusIndex; }										OnBonus;	
			struct { int ClientIndex; }														HighScoresRequest;
			struct { int ClientIndex; int Size;  }											ScoresPack;
		};
	};
};