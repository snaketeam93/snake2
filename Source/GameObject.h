#pragma once
#include<vector>

namespace GAME
{
	class GameObject
	{
		friend class Game;
	protected:
		static Game* GameObj;
	public:
		Game* GetGame() { return GameObj; }
	};
};
