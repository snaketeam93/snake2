#pragma once
#include<vector>
#include<list>
#include"Math.h"
#include<string>
#include<Windows.h>
#include<SFML\Graphics.hpp>

namespace BASE
{
	std::string FloatToString(float f);

	class DrawableObject 
	{
	protected:
		sf::RenderWindow* Window;
	public:
		void SetWindow(sf::RenderWindow* wnd) { Window = wnd; }
	};

	class Runnable
	{
		friend class Thread;
	protected:
		HANDLE hThread;
		DWORD id;
	public:
		void virtual Run() {};

		void Join();

		virtual ~Runnable() {}
	};

	class Thread
	{
	public:
		static HANDLE NewThread(Runnable* runnable);
		static HANDLE NewMutex();
		static void Wait(HANDLE mutex);
		static void Release(HANDLE mutex);
	private:
		static DWORD WINAPI ThreadFun(void* runnable);
	};

	class KeyboardListener
	{
	protected:
		static std::vector<KeyboardListener*> Listeners;
		bool Active;
	public:
		KeyboardListener() : Active(false) { Listeners.push_back(this); }

		static int GetListenersCount() { return Listeners.size(); }
		static KeyboardListener* GetListener(int i) { return Listeners[i]; }

		void StartListenKeyboard() { Active = true; }
		void StopListenKeyboard() { Active = false; }

		bool isActive() const { return Active; }

		void KeyDown(sf::Keyboard::Key key, bool shift=false, bool alt=false);
		void KeyUp(sf::Keyboard::Key key);

		virtual void OnChar(char c) {};
		virtual void OnKeyDown(sf::Keyboard::Key key) {};
		virtual void OnKeyUp(sf::Keyboard::Key key) {};
	};

	struct DataBuffer
	{
		char* Data;
		int Size;

		DataBuffer() {}
		DataBuffer(char* d, int s) : Data(d), Size(s) {}
		void Release() { delete[] Data; }
	};

	namespace Serialization
	{
		DataBuffer* SerializeStrings(std::string* strings, int count);
		void  DeserializeStrings(std::string* strings, DataBuffer& buffer);
	};
};