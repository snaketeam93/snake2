#include"Game.h"
#include "Snake.h"



using namespace std;
using namespace sf;
using namespace GAME;
using namespace MATH;

string Bonus::Names[BONUSCOUNT] = { "", "szybciej", "wolniej", "odwrotnie", "X2", "pancerz" };

ControlPoint& Snake::GetPoint(int i)
{
	//return Points[Points.size()-1-i];
	return Points[i];
}

bool Snake::ValidPoint(ControlPoint& point)
{
	bool w = ((point.Dir.x == 1.0 || point.Dir.x == -1.0) && point.Dir.y == 0.0) || ((point.Dir.y == 1.0 || point.Dir.y == -1.0) && point.Dir.x == 0.0);
	return w;
}

void Snake::EraseBack()
{
	//Points.erase(Points.begin());
	Points.pop_back();
}


void Snake::Move(int dx, int dy, bool report)
{
	if (GetPoint(0).Dir.x == -dx || GetPoint(0).Dir.y == -dy) return;

	if (CurrentBonus == Bonus::REVERSE)
	{
		dx = -dx;
		dy = -dy;
	}

	if (GetPoint(0).Dir != Vec2(dx, dy))
	{
		Moved = true;

		//Points[0].Dir = Vec2(dx, dy);

		//ControlPoint sec = GetElement(1);
		//sec.Length = (sec.Pos - Points.front().Pos).Length();

		ControlPoint cp = GetPoint(0);
		cp.Dir = Vec2(dx, dy);
		cp.Length = 0.0;
		AddElementFront(cp);

		GetPoint(1).Dir = Vec2(dx, dy);
	}

	if (CurrentBonus == Bonus::REVERSE)
	{
		dx = -dx;
		dy = -dy;
	}

	Vec2 pos = GetPoint(0).Pos;

	//GameObj->SendEvent(GameEvent(GameEvent::CONTROLPOINT, Index, Points[1].Pos.x, Points[1].Pos.y));
	if (report) GameObj->PostEvent(GameEvent(GameEvent::CHANGEDIR, Index, pos.x, pos.y, dx, dy));

	//GameObj->SendEvent(GameEvent(GameEvent::SYNCHHEAD, Index, Points[0].Pos.x, Points[0].Pos.y));
}


void Snake::Sync(const Vec2& head)
{
	Vec2 h = GetHead().Pos;
	Vec2 d = head - h;


	//Points[0].Pos = head;// d;
	for (int i=0;i<Points.size();i++)
		GetPoint(i).Pos += d;
}

void Snake::AddElementBack(ControlPoint v)
{
	Points.push_back(v);
	//Points.insert(Points.begin(), v);

	if (Points.size() > 1)
	{
		//Segments.push_back(CreateSegment(GetElement(Points.size()-2), GetElement(Points.size()-1)));
	}
}
void Snake::AddElementFront(ControlPoint v)
{
	//Points.push_back(v);
	Points.insert(Points.begin(), v);

	if (Points.size() > 1)
	{
		//Segments.push_front(CreateSegment(0), GetElement(1)));
	}
}

int Snake::GetLength(int el)
{
	if (el == -1) el = Points.size()-1;

	int l = 0;
	for (int i=0;i<Points.size();i++)
		l += GetPoint(i).Length;

	return l;
}

void Snake::UpdateLength()
{
	float prevDistance = 0.0;
	float distance = 0.0;
	int index = 0;

	for (int i=0;i<Points.size();i++)
		//for (ControlPoint& cp : Points)
	{
		ControlPoint& cp = GetPoint(i);

		prevDistance = distance;
		distance += cp.Length;

		if (distance > Length)
		{
			float d = distance - Length;

			tailMove = true;
			cp.Pos += cp.Dir*d;
			cp.Length -= d;
			return;
		}

		if (index == Points.size()-1)
		{
			if (distance < Length)
			{
				float d = distance - Length;

				//p2->Pos = p2->Pos - p2->Dir*d;
				tailMove = false;
			}
		}
		index++;
	}
}

bool Snake::Collide(const Vec2& pos, int w, int h, Vec2& pushVec)
{
	MATH::Rect r, r2;

	r2.x = pos.x+w*0.5;
	r2.y = pos.y+h*0.5;
	r2.w = w;
	r2.h = h;
	bool col = false;
	for (int i=0;i<Points.size()-1;i++)
	{
		ControlPoint& p1 = GetPoint(i);
		ControlPoint& p2 = GetPoint(i+1);

		if (p1.Pos.y == p2.Pos.y)
		{
			r.x = min(p1.Pos.x, p2.Pos.x);
			r.y = p1.Pos.y;
			r.h = Width;
			r.w = p2.Length;

			col = Test::RectRect(r, r2, pushVec);
			//pushDir = Vec2(0.0, r2.GetCenter().y > r.GetCenter().y ? 1.0 : -1.0);
		}
		else if (p1.Pos.x == p2.Pos.x)
		{
			r.x = p1.Pos.x;
			r.y = min(p1.Pos.y, p2.Pos.y);
			r.w = Width;
			r.h = p2.Length;

			col = Test::RectRect(r, r2, pushVec);

			//pushDir = Vec2(r2.GetCenter().x > r.GetCenter().x ? 1.0 : -1.0, 1.0);
		}

		if (col) return true;
	}

	return false;
}

bool Snake::AutoCollide()
{
	for (int i=3;i<Points.size()-1;i++)
	{
		ControlPoint& p1 = GetPoint(i);
		ControlPoint& p2 = GetPoint(i+1);
		const ControlPoint& head = GetHead();

		if (p1.Pos.y == p2.Pos.y)
		{
			if (min(p1.Pos.x, p2.Pos.x) < head.Pos.x && head.Pos.x < max(p1.Pos.x, p2.Pos.x) && abs(head.Pos.y-p1.Pos.y) < Snake::Width*0.8)
			{
				return true;
			}
		}
		else if (p1.Pos.x == p2.Pos.x)
		{
			if (min(p1.Pos.y, p2.Pos.y) < head.Pos.y && head.Pos.y < max(p1.Pos.y, p2.Pos.y) && abs(head.Pos.x-p1.Pos.x) < Snake::Width*0.8)
			{
				return true;
			}
		}
	}

	return false;
}

void Snake::Update(double dt)
{
	if (State == FADING)
	{
		if ((GetTickCount() - StateChangeTime) / Snake::FadingTime >= 1)
		{
			SetState(DEAD);
		}
	}
	else if (State == ALIVE)
	{
		if (CurrentBonus != Bonus::NONE)
		{
			if (GetTickCount() - BonusStartTime >= BonusTime)
			{
				StopBonus();
			}
		}


		TimeStep += dt;

		Vec2 dS = GetPoint(0).Dir*dt*Speed;
		GetPoint(0).Pos += dS;
		GetPoint(1).Length += dS.Length();

		if (tailMove)
		{
			GetPoint(Points.size()-1).Pos += GetPoint(Points.size()-1).Dir*dt*Speed;
			GetPoint(Points.size()-1).Length -= dS.Length();
		}

		TimeStep += dt;
		if (TimeStep >= 0.3)
		{
			UpdateLength();
			TimeStep = 0;
		}

		/*

		float length0 = GetLength(Points.size()-1);
		float length1 = GetLength(Points.size()-2);

		float length = Points.size() > 2 ? (length0 - length1) : last.Length;

		last.Pos = prevLast.Pos - last.Dir*length;
		last.Length = length;*/


		if (Points.size() > 2)
		{
			ControlPoint p0 = GetPoint(Points.size()-1);//Points[Points.size()-1];	//ostatni
			ControlPoint p1 = GetPoint(Points.size()-2);//Points[Points.size()-2]; // przedostatni

			if (p0.Dir.x > 0 && p0.Pos.x >= p1.Pos.x ||
				p0.Dir.x < 0 && p0.Pos.x <= p1.Pos.x ||
				p0.Dir.y > 0 && p0.Pos.y > p1.Pos.y ||
				p0.Dir.y < 0 && p0.Pos.y < p1.Pos.y)
			{
				//Points.pop_back();
				//Points.erase(Points.begin());
				EraseBack();
			}
		}

		/*
		if (TimeStep >= 1.0/Speed || Moved)
		{
		Vector2i pos = Elements.front()+Dir;
		Elements.push_front(pos);
		Elements.pop_back();

		if (Moved) TimeStep = 0;
		else TimeStep -= 1.0/Speed;

		Moved = false;
		GameObj->SendEvent(GameEvent(GameEvent::SNAKEMOVE, Index, pos.x, pos.y));
		}*/

		//_sleep(10);

	}
}


void Snake::StartBonus(Bonus::TYPE bonus)
{
	StopBonus();

	BonusStartTime = GetTickCount();

	CurrentBonus = bonus;

	if (CurrentBonus == Bonus::SPEEDUP)
	{
		Speed = DefaultSpeed*2;
	}
	else if (CurrentBonus == Bonus::SPEEDDOWN)
	{
		Speed = DefaultSpeed / 2;
	}
	else if (CurrentBonus == Bonus::FOODX2)
	{
		ScoreFactor = 2;
	}
	else if (CurrentBonus == Bonus::ARMOR)
	{
		Armor = true;
	}
}

void Snake::StopBonus()
{
	if (CurrentBonus == Bonus::SPEEDUP || CurrentBonus == Bonus::SPEEDDOWN)
	{
		Speed = DefaultSpeed;
	}
	else if (CurrentBonus == Bonus::FOODX2)
	{
		ScoreFactor = 1;
	}
	else if (CurrentBonus == Bonus::ARMOR)
	{
		Armor = false;
	}

	CurrentBonus = Bonus::NONE;
}

void Snake::Clear()
{
	Speed = DefaultSpeed;
	Moved = false;
	TimeStep = 0;
	tailMove = true;
	CurrentBonus = Bonus::NONE;
	ScoreFactor = 1;
	Armor = false;
	State = ALIVE;
	Name = "";
	Score = 0;
	Lives = 3;
	Points.clear();
}
