#pragma once
#include<vector>
#include"Math.h"
#include"GameObject.h"

namespace GAME
{
	class MapObject
	{
		friend class Map;
	public:
		static const int Width = 10;
		static const int MaxSpeed = 100;
		static const int MinSpeed = 50;

		enum TYPE { UNKNOWN, FOOD, BONUS, TRAP };

		const MATH::Vec2& GetPos() const { return Pos; }
		const MATH::Vec2 GetV()	const	{ return V; }
		TYPE GetType() const { return Type; }

		void SetPos(const MATH::Vec2& p) { Pos = p; }
		void SetV(const MATH::Vec2& v) 
		{ 
			V = v; 
		}

		virtual void OnChangeV() {}

		MapObject() : Type(UNKNOWN) {}

		void SetType(TYPE t) { Type = t; }
	protected:
		static class Map* MapObj;
		int Speed;
		int Index;

		TYPE Type;
		MATH::Vec2 Pos;
		MATH::Vec2 V;
	};

	class Map : public GameObject
	{
		friend class Game;
	private:
		int FoodCount;
		int BonusCount;
		int TrapCount;

		MATH::Vec2 Size;
		std::vector<MapObject> Objects;
	public:
		void Create(int w, int h, int maxObj=10);
		void GenerateObjects();
		void NewObject(int i, int generator, int generator2, bool throwEvent=true);
		void Update(float dt);

		MATH::Vec2			GetSize()				const { return Size;			}
		int					GetFoodCount()			const { return FoodCount;		}
		int					GetBonusCount()			const { return BonusCount;		}
		int					GetTrapCount()			const { return TrapCount;		}
		int					GetMapObjectsCount()	const { return Objects.size();	}

		const MapObject&	GetMapObject(int i)	const { return Objects[i];		}
		const MapObject&	GetFood(int i)		const { return Objects[i];		}
		const MapObject&	GetBonus(int i)		const { return Objects[i+FoodCount];		}
		const MapObject&	GetTrap(int i)		const { return Objects[i+FoodCount+BonusCount];		}

	};
};


