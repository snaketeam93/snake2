#include"Base.h"
#include"ApplicationStage.h"
#include"Application.h"
#include "Menu.h"

using namespace std;
using namespace MENU;
using namespace APPLICATION;
using namespace sf;


int modulo(int v, int m)
{
	if (m == 0 ||  v == 0) return 0;
	if (v > 0) return v%m;
	else return m - abs(v)%m;
}

void MenuEdit::UpdateLabel()
{
	ExName = Name + *Value;
	Label.setString(ExName);
}

void MenuChooser::SetValue(int v)
{
	*Value = min(Max, max(Min, v));
	ExName = Name + to_wstring(*Value);
	Label.setString(ExName);
}


void MenuStage::Init()
{
	MenuFont.loadFromFile("font.ttf");
	LogoTex.loadFromFile("gfx\\logo.png");
	LogoSprite.setTexture(LogoTex);
}

void MenuStage::OnChar(char c)
{
	if (Menu[Pos]->GetType() == MenuElement::EDIT) 
	{
		MenuEdit* me = ((MenuEdit*)Menu[Pos]);
		me->AddChar(c);
	}
}

void MenuStage::OnKeyDown(sf::Keyboard::Key k)
{
	ApplicationStage::OnKeyDown(k);

	if (k == Keyboard::Up) Pos--;
	if (k == Keyboard::Down) Pos++;
	if (k == Keyboard::Return) Choice = Pos;
	if (k == Keyboard::Left)
	{
		if (Menu[Pos]->GetType() == MenuElement::CHOOSER) 
		{
			MenuChooser* mc = ((MenuChooser*)Menu[Pos]);
			mc->SetValue(mc->GetValue()-1);
		}
	}
	if (k == Keyboard::Right)
	{
		if (Menu[Pos]->GetType() == MenuElement::CHOOSER) 
		{
			MenuChooser* mc = ((MenuChooser*)Menu[Pos]);
			mc->SetValue(mc->GetValue()+1);
		}
	}
	if (k == Keyboard::BackSpace)
	{
		if (Menu[Pos]->GetType() == MenuElement::EDIT) 
		{
			MenuEdit* me = ((MenuEdit*)Menu[Pos]);
			me->RemoveChar();
		}
	}

	Pos = modulo(Pos, Menu.size());
}

APPLICATION::ApplicationStage* MenuStage::OnUpdate(float dt)
{
	if (Choice >= 0)
	{
		if (Menu[Choice]->GetType() == MenuElement::LINK)
			return ((MenuLink*)Menu[Choice])->GetStage();
	}

	return NULL;
}

void MenuStage::Draw(int x, int y)
{
	Window->clear(Application::clearColor);

	int elx = WindowRect.w/2.0;
	int ely = WindowRect.h/2.0 - Elements.size()*50.0/2.0;

	LogoSprite.setPosition(elx-LogoSprite.getLocalBounds().width/2.0, 0);
	Window->draw(LogoSprite);

	int menuIndex = 0;
	for (int i=0;i<Elements.size();i++)
	{
		Elements[i]->Label.setColor(Color(255, menuIndex == Pos && Elements[i]->Type != MenuElement::LABEL ? 255 : 0, 0, 255));
		ely += 50;

		Elements[i]->Label.setPosition(elx+x-Elements[i]->Label.getLocalBounds().width/2.0, ely+y);

		Window->draw(Elements[i]->Label);

		if (Elements[i]->Type != MenuElement::LABEL) menuIndex++;
	}
}

void InfoStage::Draw(int x, int y)
{
	MenuStage::Draw(x, y);

	if (!MessageStr) Message.setString(*WMessageStr);
	else Message.setString(*MessageStr);

	
	Message.setPosition(WindowRect.w/2.0-Message.getLocalBounds().width/2.0+x, 200);

	Window->draw(Message);
}

void InfoStage::Init()
{
	MenuStage::Init();
	Message.setFont(MenuFont);
	Message.setColor(Color(255, 255, 0, 255));
}
