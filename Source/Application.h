#pragma once
#include"Base.h"
#include"ApplicationStage.h"
#include"BaseApplication.h"
#include"Game.h"
#include"Menu.h"
#include"..\Client\Client.h"

namespace APPLICATION
{
	class Application : public BaseApplication
	{
	private:
		CLIENT::Client SnakeClient;
		GAME::Game SnakeGame;
		MENU::MenuStage MainMenu, GameMenu, Exit;
		MENU::InfoStage HighScores;
		MENU::InfoStage Waiter;	

		std::string WaiterMessage;

		std::string ServerAdress;
		std::string ClientName;
		//MENU::MenuStage MainMenu, NewGameMenu, NewServerMenu, NewPlayerMenu, ServerSelector;
		//MENU::ServerConnectionStage ServerConStage;
	public:
		static const sf::Color clearColor;

		Application(void) : ServerAdress(""), ClientName("") {}
		~Application(void) {}

		void OnUpdate(float dt);
		void OnStageChange(ApplicationStage* prev, ApplicationStage* current);
		void OnInit();
		void OnRelease();

	};
}