#include<Windows.h>
#include"Timer.h"

using namespace BASE;

float Timer:: GetSecs()
{
	LARGE_INTEGER uFreq;   
   if (!(QueryPerformanceFrequency (&uFreq) != 0))   return GetTickCount() / 1000.0f;
   else
   {
      LARGE_INTEGER uTicks;
      QueryPerformanceCounter (&uTicks);
      return (float)(uTicks.QuadPart / (double)uFreq.QuadPart);
   }
}


void Timer::Start()
{
	if (State == ST_RUN) return;
	StartTime = GetSecs();
	Time = 0.0f;

	State = ST_RUN;
}

void Timer::Update()
{
	static float fFpsTime;
	if (State != ST_RUN) return;

	LastTime = Time;
	Time = GetSecs()-StartTime;
	Dt = Time - LastTime;
	Frames++;

	fFpsTime +=  Dt;
	if (fFpsTime >= 1.0f)
	{
		FPS = Frames / fFpsTime;
		Frames = 0.0f;
		fFpsTime = 0.0f;
	}
}

void Timer::Stop()
{
	State = ST_PAUSE;
}


	
