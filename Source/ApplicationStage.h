#pragma once

namespace APPLICATION
{
	class ApplicationStage : public BASE::DrawableObject, public BASE::KeyboardListener
	{
		friend class BaseApplication;
	protected:
		static MATH::Rect WindowRect;

		ApplicationStage* Parent;
		bool Escape;
	public:
		static void SetWindowRect(const MATH::Rect& w) { WindowRect = w; }

		ApplicationStage() : Escape(false), Parent(NULL) {}

		virtual void OnKeyDown(sf::Keyboard::Key key);
		virtual void OnKeyUp(sf::Keyboard::Key key) {};

		void SetParent(ApplicationStage* stage) { Parent = stage; }
		void Open();
		void Close();
		ApplicationStage* Update(float dt);
		ApplicationStage* GetParent() { return Parent; }

		virtual void OnEscape() { if (Parent) Escape = true; }
		virtual void Init() {}
		virtual void Release() {}
		virtual void OnOpen() { Escape = false; }
		virtual ApplicationStage* OnUpdate(float dt) { return NULL; }
		virtual void Draw(int x, int y) {}
	};
}