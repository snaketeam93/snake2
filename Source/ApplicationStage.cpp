#include"Base.h"
#include"ApplicationStage.h"

using namespace APPLICATION;

MATH::Rect ApplicationStage::WindowRect;

void ApplicationStage::OnKeyDown(sf::Keyboard::Key key) 
{ 
	if (key == sf::Keyboard::Escape) OnEscape(); 
};
		

void ApplicationStage::Open() 
{ 
	Escape = false; 
	StartListenKeyboard();
	OnOpen(); 
}

void ApplicationStage::Close()
{
	StopListenKeyboard();
}

ApplicationStage* ApplicationStage::Update(float dt) 
{ 
	if (Escape) return Parent; 
	else return OnUpdate(dt); 
}