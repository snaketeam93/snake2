#pragma once
#include"Network.h"
#include<vector>
#include"Snake.h"
#include"Base.h"
#include<SFML\Window.hpp>


namespace GAME
{
	class Controller
	{
	public:
		enum TYPE { LOCAL, REMOTE, COMPUTER, NUMCONTROLLERS };

		void AddSnake(Snake& snake);
		void SetControls(sf::Keyboard::Key left, sf::Keyboard::Key right, sf::Keyboard::Key up, sf::Keyboard::Key down);

		Controller(TYPE t) : Type(t) {}
		virtual ~Controller(void) {}
	protected:
		void MoveSnakes(int dx, int dy);

		sf::Keyboard::Key Controls[4];
		std::vector<Snake*> Snakes;
		TYPE Type;
	};

	class LocalController : public Controller, public BASE::KeyboardListener
	{
	private:
		
	public:
		LocalController() : Controller(Controller::LOCAL) {}

		void OnKeyDown(sf::Keyboard::Key key);
	};

	/*
	class RemoteControllerReceiver : public Controller, public NETWORK::SocketListener
	{
	public:
		RemoteControllerReceiver() : Controller(Controller::REMOTE) {}

		void OnReceive(char* buffer, int size);
		void OnSend(char* data, int size) {}
	};*/

	/*
	class RemoteControllerSender : public Controller, public CLIENT::KeyboardListener
	{
	private:
		NETWORK::Socket* Receiver;
	public:
		RemoteControllerSender() : Controller(Controller::REMOTE) {}

		void SetReceiver(NETWORK::Socket* receiver) { Receiver = receiver; }
		void OnKeyDown(sf::Keyboard::Key key);
	};*/

	/*
	class ComputerController : public Controller
	{
	public:
		ComputerController() : Controller(Controller::COMPUTER) {}
	};*/
};



