#pragma once

namespace MENU
{
	class MenuElement
	{
		friend class MenuStage;
	public:
		enum TYPE { LINK, CHOOSER, EDIT, LABEL };
		TYPE GetType() const { return Type; }
		std::wstring GetName() { return Name; }

		MenuElement(TYPE t, const std::wstring& name) : Name(name), Type(t) { Label.setString(name); }
		virtual ~MenuElement() {}
	protected:
		sf::Text Label;
		std::wstring Name;
		TYPE Type;
	};

	class MenuLabel : public MenuElement
	{
	public:
		MenuLabel(const std::wstring& name) : MenuElement(MenuElement::LABEL, name) {}
	};

	class MenuLink : public MenuElement
	{
	private:
		APPLICATION::ApplicationStage* Stage;
	public:
		MenuLink(const std::wstring& name, APPLICATION::ApplicationStage* stage) : MenuElement(MenuElement::LINK, name), Stage(stage) {}

		APPLICATION::ApplicationStage* GetStage() { return Stage; }
	};

	class MenuChooser : public MenuElement
	{
	private:
		int Min, Max;
		int* Value;
		std::wstring ExName;
	public:
		MenuChooser(const std::wstring& name, int min, int max, int* val) : MenuElement(MenuElement::CHOOSER, name), Min(min), Max(max) 
		{
			Value = val;
			SetValue(*val);
		}

		int GetMin() const { return Min; }
		int GetMax() const { return Max; }
		void SetValue(int v);
		int GetValue() { return *Value; }
	};

	class MenuEdit : public MenuElement
	{
	private:
		std::string* Value;
		std::wstring ExName;
	public:
		MenuEdit(const std::wstring& name, std::string* val) : MenuElement(MenuElement::EDIT, name), Value(val) {} 

		void UpdateLabel();
		void AddChar(char c) { Value->push_back(c); UpdateLabel(); }
		void RemoveChar() { if (Value->size()) { Value->erase(Value->begin()+Value->size()-1); UpdateLabel(); } }
	};


	class MenuStage : public APPLICATION::ApplicationStage
	{
	protected:
		std::vector<MenuElement*> Menu;
		std::vector<MenuElement*> Labels;
		sf::Color ClearColor;

		std::vector<MenuElement*> Elements;

		sf::Font MenuFont;
		sf::Texture LogoTex;
		sf::Sprite LogoSprite;


		int Pos;
		int Choice;
	public:
		MenuStage(void) : Pos(0), Choice(-1) {}
		~MenuStage(void) {}

		void OnChar(char c);
		void OnKeyDown(sf::Keyboard::Key k);
		void OnKeyUp(sf::Keyboard::Key k) {}

		void AddMenuElement(MenuElement* menuEl) 
		{ 
			if (menuEl->Type == MenuElement::LABEL) 
				Labels.push_back(menuEl);
			else 
				Menu.push_back(menuEl); 
			
			menuEl->Label.setFont(MenuFont);
			Elements.push_back(menuEl);
		}
		
		void Init();
		void Release() { for (int i=0;i<Elements.size();i++) { delete Elements[i]; }}
		void OnOpen() { Choice = -1; }
		APPLICATION::ApplicationStage* OnUpdate(float dt);
		void Draw(int x, int y);
	};

	class InfoStage : public MenuStage
	{
	protected:
		sf::Text Message; 
		const std::wstring* WMessageStr;
		const std::string* MessageStr;
	public:
		InfoStage(void) : WMessageStr(0), MessageStr(0) {}
		~InfoStage(void) {}

		void Init();

		void SetMessage(const std::wstring* msg) { WMessageStr = msg; }
		void SetMessage(const std::string* msg) { MessageStr = msg; }

		APPLICATION::ApplicationStage* OnUpdate(float dt) { return NULL; }
		void Draw(int x, int y);
	};
};

