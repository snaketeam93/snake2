#include"Base.h"
#include"Math.h"
#include<math.h>
#include<algorithm>
#include<sstream>


using namespace std;
using namespace BASE;

//sf::RenderWindow* DrawableObject::Window;
vector<KeyboardListener*> KeyboardListener::Listeners;

string BASE::FloatToString(float f)
{
	stringstream stream;
	stream << f;
	return stream.str();
}

HANDLE Thread::NewThread(Runnable* runnable)
{
	runnable->hThread = CreateThread(NULL, 0, Thread::ThreadFun, (void*)runnable, 0, &(runnable->id));
	return runnable->hThread;
}

HANDLE Thread::NewMutex()
{
	return CreateMutex(NULL, FALSE, NULL);
}

void Thread::Wait(HANDLE mutex)
{
	WaitForSingleObject(mutex, INFINITE);
}

void Thread::Release(HANDLE mutex)
{
	ReleaseMutex(mutex);
}

void Runnable::Join()
{
	WaitForSingleObject(hThread, INFINITE);
}

DWORD Thread::ThreadFun(void* runnable)
{
	((Runnable*)runnable)->Run();
	return 0;
}

bool MATH::Test::RectPoint(const Rect& r, const Vec2& v)
{
	return (v.x > r.x && v.x < r.x+r.w && v.y > r.y && v.y < r.y+r.h);
}

float sign(float v)
{
    return v / abs(v);
}

bool MATH::Test::RectRect(const Rect& r, const Rect& r2, Vec2& pushVector)
{
    float right1 = r.x+r.w;
    float bottom1 = r.y+r.h;
    float right2 = r2.x+r2.w;
    float bottom2 = r2.y+r2.h;

    float xAxis = max(right1, right2) - min(r.x, r2.x);
    float yAxis = max(bottom1, bottom2) - min(r.y, r2.y);

    if (xAxis < r.w+r2.w &&
        yAxis < r.h+r2.h)
    {
        float xOverlap = (r.w+r2.w - xAxis)*sign(r2.x-r.x);
        float yOverlap = (r.h+r2.h - yAxis)*sign(r2.y-r.y);

        if (abs(xOverlap - yOverlap) < 0.001) pushVector = Vec2(xOverlap, yOverlap);
        else
        {
            if (xOverlap > yOverlap) pushVector = Vec2(0.0, yOverlap);
            else pushVector = Vec2(xOverlap, 0.0);
        }

        return true;
    }

    return false;
}

void KeyboardListener::KeyDown(sf::Keyboard::Key key, bool shift, bool alt)
{
	OnKeyDown(key);

	char c = -1;
	if (key == sf::Keyboard::Space) c = ' ';
	else if (key == sf::Keyboard::A) c = 'a';
	else if (key == sf::Keyboard::B) c = 'b';
	else if (key == sf::Keyboard::C) c = 'c';
	else if (key == sf::Keyboard::D) c = 'd';
	else if (key == sf::Keyboard::E) c = 'e';
	else if (key == sf::Keyboard::F) c = 'f';
	else if (key == sf::Keyboard::G) c = 'g';
	else if (key == sf::Keyboard::H) c = 'h';
	else if (key == sf::Keyboard::I) c = 'i';
	else if (key == sf::Keyboard::J) c = 'j';
	else if (key == sf::Keyboard::K) c = 'k';
	else if (key == sf::Keyboard::L) c = 'l';
	else if (key == sf::Keyboard::M) c = 'm';
	else if (key == sf::Keyboard::N) c = 'n';
	else if (key == sf::Keyboard::O) c = 'o';
	else if (key == sf::Keyboard::P) c = 'p';
	else if (key == sf::Keyboard::Q) c = 'q';
	else if (key == sf::Keyboard::R) c = 'r';
	else if (key == sf::Keyboard::S) c = 's';
	else if (key == sf::Keyboard::T) c = 't';
	else if (key == sf::Keyboard::U) c = 'u';
	else if (key == sf::Keyboard::V) c = 'v';
	else if (key == sf::Keyboard::W) c = 'w';
	else if (key == sf::Keyboard::X) c = 'x';
	else if (key == sf::Keyboard::Y) c = 'y';
	else if (key == sf::Keyboard::Z) c = 'z';
	else if (key == sf::Keyboard::Num0) c = '0';
	else if (key == sf::Keyboard::Num1) c = '1';
	else if (key == sf::Keyboard::Num2) c = '2';
	else if (key == sf::Keyboard::Num3) c = '3';
	else if (key == sf::Keyboard::Num4) c = '4';
	else if (key == sf::Keyboard::Num5) c = '5';
	else if (key == sf::Keyboard::Num6) c = '6';
	else if (key == sf::Keyboard::Num7) c = '7';
	else if (key == sf::Keyboard::Num8) c = '8';
	else if (key == sf::Keyboard::Num9) c = '9';
	else if (key == sf::Keyboard::Comma) c = ',';
	else if (key == sf::Keyboard::Period) c = '.';

	if (shift) c = toupper(c);


	if (c != -1) OnChar(c);
}

void KeyboardListener::KeyUp(sf::Keyboard::Key key)
{
	OnKeyUp(key);
}

DataBuffer* Serialization::SerializeStrings(string* strings, int count)
{
	int size = 0;
	for (int i=0;i<count;i++)
	{
		size += strings[i].size();
	}

	int ultimateSize = size+(count+1)*sizeof(int);
	char* buffer = new char[ultimateSize];

	int pos = 0;
	buffer[pos] = count;
	pos += sizeof(int);
	for (int i=0;i<count;i++)
	{
		buffer[pos] = strings[i].size();
		pos += sizeof(int);
		for (int j=0;j<strings[i].size();j++)
		{
			buffer[pos++] = strings[i][j];
		}
	}

	return new DataBuffer(buffer, ultimateSize);
}

void Serialization::DeserializeStrings(string* strings, DataBuffer& datBuff)
{
	int pos = 0;
	char* buffer = datBuff.Data;
	int count = buffer[pos];
	pos += sizeof(int);

	for (int i=0;i<count;i++)
	{
		int length = buffer[pos];
		pos += sizeof(int);

		for (int j=0;j<length;j++)
		{
			strings[i].push_back(buffer[pos++]);
		}
	}
}