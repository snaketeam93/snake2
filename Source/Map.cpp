#include "Map.h"
#include"Game.h"

using namespace GAME;
using namespace MATH;

Map* MapObject::MapObj = NULL;

void Map::Create(int w, int h, int maxObj)
{
	MapObject::MapObj = this;
	Size = Vec2(w, h);
	Objects.resize(maxObj);
	for (int i=0;i<maxObj;i++) Objects[i].Index = i;

	FoodCount = maxObj*0.5;
	BonusCount = FoodCount*(3.0/(float)FoodCount);
	TrapCount = maxObj - (BonusCount+FoodCount);

	for (int i=0;i<FoodCount;i++) Objects[i].Type = MapObject::FOOD;
	for (int i=0;i<BonusCount;i++) Objects[i+FoodCount].Type = MapObject::BONUS;
	for (int i=0;i<TrapCount;i++) Objects[i+FoodCount+BonusCount].Type = MapObject::TRAP;
}

void Map::GenerateObjects()
{
	srand(time(NULL));
	for (int i=0;i<Objects.size();i++)
	{
		NewObject(i, rand(), rand());
	}
}

void Map::NewObject(int i, int generator, int generator2, bool throwEvent)
{
	float x  = generator%(int)(Size.x-2*MapObject::Width)+MapObject::Width;
	float y  = generator2%(int)(Size.y-2*MapObject::Width)+MapObject::Width;

	Objects[i].Pos = Vec2(x, y);
	Objects[i].Speed = (generator*generator2)%(MapObject::MaxSpeed-MapObject::MinSpeed)+MapObject::MinSpeed;
	Objects[i].SetV((Vec2(generator2, generator))/(0.5*RAND_MAX) - Vec2(1.0f, 1.0f));

	if (throwEvent)
	{
		GameObj->PostEvent(GameEvent(GameEvent::NEWOBJECT, i, generator, generator2));
	}
}

void Map::Update(float dt)
{
	
	for (int i=0;i<Objects.size();i++)
	{
		MapObject& obj = Objects[i];

		Vec2 ds = obj.V * dt * obj.Speed; 
		obj.Pos += ds;

		if (obj.Pos.x < 0 || obj.Pos.x+MapObject::Width > Size.x) { obj.V.x = -obj.V.x; obj.Pos -= ds; }
		if (obj.Pos.y < 0 || obj.Pos.y+MapObject::Width > Size.y) { obj.V.y = -obj.V.y; obj.Pos -= ds; }

		/*
		for (MapObject& fo2 : Objects)
		{
			if (&obj == &fo2) continue;

			if ((obj.Pos - fo2.Pos).Length() <= MapObject::Width)
			{
				Vec2 v1 = (obj.Pos - fo2.Pos).Normalize();
				Vec2 v2 = obj.V.Normalize(); 

				obj.V = (v1*2 + v2).Normalize();

				v1 = -v1;
				v2 = fo2.V.Normalize(); 

				fo2.V = (v1*2 + v2).Normalize();

				obj.Pos -= ds;
			}
		}*/
	}
}


