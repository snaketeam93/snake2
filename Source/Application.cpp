#include"Application.h"

using namespace APPLICATION;
using namespace MENU;


void Application::OnInit()
{
	AppWindow.setFramerateLimit(60);
	
	MainMenu.Init();
	MainMenu.AddMenuElement(new MenuEdit(L"Nazwa: ", &ClientName));
	MainMenu.AddMenuElement(new MenuEdit(L"Serwer: ", &ServerAdress));
	MainMenu.AddMenuElement(new MenuLink(L"Po��cz", &GameMenu));
	MainMenu.AddMenuElement(new MenuLink(L"Wyj�cie", &Exit));
	MainMenu.SetWindow(&AppWindow);

	GameMenu.Init();
	GameMenu.AddMenuElement(new MenuLink(L"Graj", &Waiter));
	GameMenu.AddMenuElement(new MenuLink(L"Rekordy", &HighScores));
	GameMenu.AddMenuElement(new MenuLink(L"Roz��cz", &MainMenu));
	GameMenu.SetWindow(&AppWindow);

	WaiterMessage = "Czekam na innych graczy";
	GameMenu.SetParent(&MainMenu);
	Waiter.Init();
	Waiter.SetParent(&GameMenu);
	Waiter.SetMessage(&WaiterMessage);
	Waiter.SetWindow(&AppWindow);

	HighScores.SetParent(&GameMenu);
	HighScores.Init();
	HighScores.SetWindow(&AppWindow);
	HighScores.SetMessage(&SnakeClient.GetScoresString());

	SnakeGame.BaseInit();
	SnakeGame.Init();
	SnakeGame.SetParent(&GameMenu);
	SnakeGame.SetWindow(&AppWindow);

	SetActiveStage(&MainMenu);
}

void Application::OnStageChange(ApplicationStage* prev, ApplicationStage* current)
{
	if (current == &GameMenu && prev == &MainMenu)
	{
		if (!SnakeClient.Init(SnakeGame, ServerAdress, ClientName))
		{
			SetActiveStage(current->GetParent());
		}
	}
	if (current == &Exit)
	{
		AppWindow.close();
	}
	if (current == &HighScores && prev == &GameMenu)
	{
		SnakeClient.SendScores();
		SnakeClient.HighScoresRequest();
	}
	if (current == &MainMenu && prev == &GameMenu)
	{
		SnakeClient.Release();
	}
	if (current == &Waiter && prev == &GameMenu)
	{
		SnakeClient.StartGameRequest();
	}
	if (current == &GameMenu && prev == &SnakeGame)
	{
		SnakeClient.SaveGameScores();
		SnakeGame.Release();
	}
}
	
void Application::OnRelease()
{
	MainMenu.Release();
	GameMenu.Release();
	Waiter.Release();

	SnakeGame.Release();
	SnakeClient.Release();
}

void Application::OnUpdate(float dt)
{

	//if (SnakeClient.Running())
	{
		//SnakeClient.ProcessEvents();
	}

	if (SnakeGame.Exist())
	{
		//SnakeGame.CheckCollisions();
	}

	if (ActiveStage == &Waiter)
	{
		if (SnakeGame.Exist())
		{
			SetActiveStage(&SnakeGame);
		}
	}
}


