#include"Application.h"
#include"Game.h"
//#include<thread>
#include<math.h>
#include<algorithm>

using namespace NETWORK;
using namespace MATH;
using namespace GAME;
using namespace std;
using namespace sf;

StartState Game::StartPoints[4];
Game* GameObject::GameObj = NULL;
vector<GameEventListener*> GameEventListener::Listeners;
const sf::Color APPLICATION::Application::clearColor = sf::Color(0, 162, 232, 255);

const sf::Color Game::mapColor = Color(0, 100, 0, 255);
const sf::Color Game::foodColor = Color(255, 255, 0, 255);
const sf::Color Game::bonusColor = Color(255, 0, 0, 255);
const sf::Color Game::trapColor = Color(0, 0, 0, 255);
const sf::Color Game::blackColor = Color(0, 0, 0, 255);


void Game::Update(GameEvent ev)
{
	//if (bEnd) return;
	if (ev.Type == GameEvent::CLIENTNAME)
	{
		//Snakes[ev.nameForClient].SetPlayerName
	}
	if (ev.Type == GameEvent::CHANGEDIR)
	{
		//Block();

		if (ev.ChangeDir.SnakeIndex< Snakes.size())
		{
			Snakes[ev.ChangeDir.SnakeIndex].GetPoint(0).Pos.x = ev.ChangeDir.PosX;
			Snakes[ev.ChangeDir.SnakeIndex].GetPoint(0).Pos.y = ev.ChangeDir.PosY;
			Snakes[ev.ChangeDir.SnakeIndex].Move(ev.ChangeDir.DirX, ev.ChangeDir.DirY, false);

			//Snakes[ev.snakeIndex].Move(ev.dx, ev.dy);
		}

		//Unblock();
	}
	if (ev.Type == GameEvent::SYNCHOBJECTS)
	{
		int k = 0;
		for (int i=0;i<MapObj.Objects.size();i++)
		{
			MapObj.Objects[i].SetPos(Vec2(ev.SyncObjects.Data[k], ev.SyncObjects.Data[k+1]));
			MapObj.Objects[i].SetV(Vec2(ev.SyncObjects.Data[k+2], ev.SyncObjects.Data[k+3]));
			k += 4;
		}

		//MapObj.Update((double)ev.syncTime/1000.0);

		delete[] ev.SyncObjects.Data;
	}
	if (ev.Type == GameEvent::NEWOBJECT)
	{
		MapObj.NewObject(ev.NewObject.ObjectIndex, ev.NewObject.Generator1, ev.NewObject.Generator2);
	}
	if (ev.Type == GameEvent::ONSCORE)
	{
		Snakes[ev.OnScore.SnakeIndex].Length += 10;
		Snakes[ev.OnScore.SnakeIndex].Score += ev.OnScore.Score;

		char buff[4];
		sprintf(buff, "+%d", ev.OnScore.Score);
		Notifications.push_back(Notification(Snakes[ev.OnScore.SnakeIndex].GetHead().Pos, string(buff), GetTickCount())); 
	}
	if (ev.Type == GameEvent::SNAKEDEATH)
	{
		Snakes[ev.SnakeDeath.SnakeIndex].SetState(Snake::FADING);
	}
	if (ev.Type == GameEvent::ONBONUS)
	{
		Notifications.push_back(Notification(Snakes[ev.OnBonus.SnakeIndex].GetHead().Pos, Bonus::Names[ev.OnBonus.BonusIndex], GetTickCount())); 
		Snakes[ev.OnBonus.SnakeIndex].StartBonus((Bonus::TYPE)ev.OnBonus.BonusIndex);
	}
	if (ev.Type == GameEvent::CLIENTINDEX)
	{
		ArrowController.AddSnake(GetSnake(ev.ClientIndex.ClientIndex));
		WSADController.AddSnake(GetSnake(ev.ClientIndex.ClientIndex));
	}
}


void Game::PostEvent(GameEvent gameEvent)
{
	for (int i=0;i<GameEventListener::Listeners.size();i++)
	{
		GameEventListener::Listeners[i]->OnReceiveGameEvent(gameEvent);
	}
}

void Game::BuildScores()
{
	for (int i=0;i<Scores.size();i++)
	{
		Scores[i] = Score(Snakes[i].Name, Snakes[i].Score);
	}

	sort(Scores.begin(), Scores.end(), Score::Compare);

	char buff[5];
	ScoresString = "Wyniki:\n";
	for (int i=0;i<Scores.size();i++)
	{
		ScoresString += Scores[i].Name + " ";

		itoa(Scores[i].Value, buff, 10);
		ScoresString += string(buff) + "\n";
	}
}

void Game::Create(int numPlayers, int maxFood, string* names)
{
	bEnd = false;
	Scores.resize(numPlayers);
	State = GAME;
	PlayersCount = numPlayers;
	Blocked = false;

	float xOffset = WindowRect.w*0.2;
	float yOffset = WindowRect.h*0.2;
	ScoresRect.setPosition(xOffset, yOffset);
	ScoresRect.setSize(Vector2f(WindowRect.w-2.0*xOffset, WindowRect.h-2.0*yOffset));
	ScoresRect.setFillColor(Color(0, 0, 0, 64));

	Vec2 mapSize = Vec2(MapW, MapH);
	Vec2 startPadding = Vec2(50, 20);

	StartPoints[0].Pos = startPadding;
	StartPoints[0].Dir = Vec2(1, 0);
	StartPoints[1].Pos = Vec2(mapSize.x-startPadding.x, 1);
	StartPoints[1].Dir = Vec2(0, 1);
	StartPoints[2].Pos = mapSize - startPadding;
	StartPoints[2].Dir = Vec2(-1, 0);
	StartPoints[3].Pos = Vec2(1, mapSize.y-startPadding.y);
	StartPoints[3].Dir = Vec2(0, -1);

	SnakesColors[0] = Color(255, 0, 0, 255);
	SnakesColors[1] = Color(0, 255, 0, 255);
	SnakesColors[2] = Color(0, 0, 255, 255);
	SnakesColors[3] = Color(255, 255, 0, 255);

	MapObj.Create(mapSize.x, mapSize.y, maxFood);

	Snakes.clear();
	Snakes.resize(numPlayers);
	for (int i=0;i<numPlayers;i++)
	{
		Snakes[i].Clear();
		Snakes[i].Index = i;
		int startLength = 30;
		int startWidth = 10;

		Snakes[i].Name = names[i];
		Snakes[i].SetLength(startLength);
		Snakes[i].AddElementBack(ControlPoint(StartPoints[i].Pos, StartPoints[i].Dir, 0));
		Snakes[i].AddElementBack(ControlPoint(StartPoints[i].Pos-StartPoints[i].Dir*startLength, StartPoints[i].Dir, startLength));

		//for (int j=startLength-1;j>=0;j--)
		{
			//	Snakes[i].Elements.push_back(Vector2i(StartPoints[i].Pos+StartPoints[i].Dir*j));
		}

		Snakes[i].Col = SnakesColors[i];
	}
}

void Game::End()
{
	bEnd = true;
	PostEvent(GameEvent(GameEvent::CLOSEGAME));
}

void Game::ScoresToString(std::vector<Score>& scores, std::string& output)
{
	char buff[10];
	output.clear();
	for (int i=0;i<scores.size();i++)
	{
		output += scores[i].Name + " ";

		itoa(scores[i].Value, buff, 10);
		output += string(buff) + "\n";
	}
}

void Game::SendScores()
{	

}

void Game::BaseInit()
{
	GameObject::GameObj = this;
	Margin = 10;
	MapW = 0.78*WindowRect.w;
	MapH = WindowRect.h-2*Margin;

	ArrowController.SetControls(Keyboard::Left, Keyboard::Right, Keyboard::Up, Keyboard::Down);
	WSADController.SetControls(Keyboard::A, Keyboard::D, Keyboard::W, Keyboard::S);
	ArrowController.StartListenKeyboard();
	WSADController.StartListenKeyboard();
	GameMutex = BASE::Thread::NewMutex();


	if (!GameFont.loadFromFile("font.ttf"))
	{
		//MessageBox(0, "Nie mo�na otworzy� fonta", "error", 0);
	}
}


void Game::CheckCollisions()
{
	//Block();

	//jedzenie
	Vec2 pushVec;

	for (int k=0;k<Snakes.size();k++)
	{
		if (!Snakes[k].IsAlive()) continue;
		ControlPoint head = Snakes[k].GetHead();

		if ((head.Pos.x < 0 || head.Pos.y < 0 || head.Pos.x > MapObj.GetSize().x || head.Pos.y > MapObj.GetSize().y) && Snake::ValidPoint(head))
		{
			Snakes[k].SetState(Snake::FADING);
			PostEvent(GameEvent(GameEvent::SNAKEDEATH, k));
		}


		for (int p=0;p<Snakes.size();p++)
		{
			if (!Snakes[p].IsAlive()) continue;

			if ((p != k && (Snakes[p].Collide(head.Pos - Vec2(Snake::Width/2.0, Snake::Width/2.0), Snake::Width, Snake::Width, pushVec)) || Snakes[k].AutoCollide()) && !Snakes[k].Armor)
			{
				GameEvent ev = GameEvent(GameEvent::SNAKEDEATH, k);
				PostEvent(ev);
				Update(ev);
				return;
			}
		}

		for (int i=0;i<MapObj.GetMapObjectsCount();i++)
		{
			Snake& snake = Snakes[k];
			MapObject obj = MapObj.Objects[i];

			Vec2 p = snake.GetPoint(0).Pos;
			MATH::Rect r = MATH::Rect(p.x - snake.Width*0.5, p.y - snake.Width*0.5, snake.Width, snake.Width);
			Vec2 pos = obj.GetPos() + Vec2(MapObject::Width/2, MapObject::Width/2);
			MATH::Rect r2 = MATH::Rect(pos.x - snake.Width*0.5, pos.y - snake.Width*0.5, snake.Width, snake.Width);

			if (MATH::Test::RectRect(r, r2, pos))
			{
				if (obj.GetType() == MapObject::FOOD)
				{
					GameEvent gevent = GameEvent(GameEvent::ONSCORE, k, FoodScore*snake.ScoreFactor);

					Update(gevent);
					PostEvent(gevent);
					MapObj.NewObject(i, rand(), rand());
				}
				if (obj.GetType() == MapObject::BONUS)
				{
					GameEvent gevent = GameEvent(GameEvent::ONBONUS, k, RandomBonus());

					PostEvent(gevent);
					Update(gevent);

					MapObj.NewObject(i, rand(), rand());
				}
				if (obj.GetType() == MapObject::TRAP && !snake.Armor)
				{
					Snakes[k].SetState(Snake::FADING);
					PostEvent(GameEvent(GameEvent::SNAKEDEATH, k));
				}
			}

			/*
			if (Snakes[k].Collide(MapObj.Objects[i].GetPos(), 10, 10, pushVec))
			{
			Vec2 dirVec(1.0, 1.0);
			if (pushVec.y != 0.0) dirVec.y = -1;
			if (pushVec.x != 0.0) dirVec.x = -1;

			MapObj.Objects[i].SetPos(MapObj.Objects[i].GetPos()+pushVec);
			MapObj.Objects[i].SetV(MapObj.Objects[i].GetV()*dirVec);
			}*/
		}
	}

	//Unblock();
}

void Game::DrawNotifications(int x, int y)
{
	for (int i=0;i<Notifications.size();i++)
	{
		text.setString(Notifications[i].Text);
		text.setPosition(Notifications[i].Pos.x+x, Notifications[i].Pos.y+y);
		int alpha = (1.0f - ((float)(GetTickCount() - Notifications[i].StartTime) / (float)NotificationTime)) * 255;
		text.setColor(Color(255, 255, 0, alpha));
		Window->draw(text);
	}
}

void Game::DrawSnakeUI(int snake, int x, int y)
{
	float w = WindowRect.w - MapW - 3*Margin;
	float h = (WindowRect.h - 4*Margin)/4;

	UIrect.setSize(Vector2f(w, h));
	UIrect.setPosition(x, y);
	UIrect.setFillColor(SnakesColors[snake]);
	//rect.setOutlineThickness(2);
	//rect.setOutlineColor(Snakes[snake].Col);
	Window->draw(UIrect);

	if (snake >= Snakes.size()) return;

	text.setColor(SnakesColors[(snake == 0 ? 3 : (snake-1)%4)]);
	text.setFont(GameFont);
	text.setString(Snakes[snake].Name);
	text.setPosition(x+10, y+10);

	Window->draw(text);

	char buff[10];
	itoa(Snakes[snake].Score, buff, 10);
	text.setString(buff);
	text.setPosition(x+10, y+50);
	Window->draw(text);

	if (Snakes[snake].CurrentBonus != Bonus::NONE)
	{
		bar.setSize(Vector2f(w-10, 20));
		bar.setPosition(x+5, y+h-25);
		bar.setFillColor(blackColor);

		float progress = (float)(GetTickCount() - Snakes[snake].BonusStartTime) / (float)Snake::BonusTime;

		innerBar.setSize(Vector2f(progress*(w-14), 16));
		innerBar.setPosition(x+7, y+h-23);
		innerBar.setFillColor(SnakesColors[(snake+1)%4]);

		Window->draw(bar);
		Window->draw(innerBar);

		text.setString(Bonus::Names[Snakes[snake].CurrentBonus]);
		text.setPosition(x+10, y+80);
		Window->draw(text);
	}
}

void Game::DrawMap(int x, int y)
{
	mapRect.setSize(Vector2f(MapObj.Size.x, MapObj.Size.y));
	mapRect.setPosition(x, y);
	mapRect.setFillColor(mapColor);
	Window->draw(mapRect);

	objCircle.setRadius(MapObject::Width/2);
	objCircle.setOutlineColor(blackColor);
	objCircle.setOutlineThickness(2);

	objCircle.setFillColor(foodColor);
	for (int i=0;i<MapObj.Objects.size();i++)
	{
		if (i == MapObj.FoodCount) objCircle.setFillColor(bonusColor);
		else if (i == MapObj.FoodCount+MapObj.BonusCount) objCircle.setFillColor(trapColor);

		Vec2 pos = MapObj.GetMapObject(i).GetPos();
		objCircle.setPosition(pos.x+x, pos.y+y);
		Window->draw(objCircle);
	}
}

void DrawLine(Vec2& p1, Vec2& p2, int width, RenderWindow* wnd)
{

}

void Game::DrawSnakes(int x, int y)
{
	//---------------------------
	//RYSOWANIE PUNKTOW ZGIECIA
	//---------------------------
	/*
	float r = 6.0;
	CircleShape circleShape(r);
	circleShape.setFillColor(sf::Color(0, 0, 255, 255));
	for (Snake& s : Snakes)
	{
	for (ControlPoint& cp : s.Points)
	{
	circleShape.setPosition(cp.Pos.x-r*0.5, cp.Pos.y-r*0.5);
	Window->draw(circleShape);
	}
	}*/

	int border = 2;
	rectShape.setFillColor(blackColor);

	for (int i=0;i<Snakes.size();i++)
	{
		Snake& s = Snakes[i];

		float h = (WindowRect.h - 3*Margin)/4;
		DrawSnakeUI(s.Index, MapW+Margin*2, (h+Margin)*s.Index+Margin);

		if (s.IsDead()) continue;


		if (s.CurrentBonus == Bonus::ARMOR && GetTickCount() % 250 < 125) continue;

		for (int i=0;i<s.Points.size()-1;i++)
		{
			Vec2 p1 = s.GetPoint(i).Pos;
			Vec2 p2 = s.GetPoint(i+1).Pos;

			int l = (p1 - p2).Length()+s.Width;

			if (p1.x == p2.x) rectShape.setSize(Vector2f(s.Width, l));
			else rectShape.setSize(Vector2f(l, s.Width));

			if (s.State == Snake::FADING)
			{
				int alpha =  (1.0f - (float)(GetTickCount() - s.GetStateChangeTime()) / (float)Snake::FadingTime) * 255;
				rectShape.setFillColor(Color(0, 0, 0, alpha));
			}
			else
			{
				rectShape.setFillColor(Game::blackColor);
			}

			rectShape.setPosition(min(p1.x, p2.x)-s.Width/2+x, min(p1.y, p2.y)-s.Width/2+y);
			Window->draw(rectShape);
		}

		for (int i=0;i<s.Points.size()-1;i++)
		{
			Vec2 p1 = s.GetPoint(i).Pos;
			Vec2 p2 = s.GetPoint(i+1).Pos;

			int l = (p1 - p2).Length()+s.Width;
			int border = 2;

			if (p1.x == p2.x) innerRectShape.setSize(Vector2f(s.Width-border*2, l-border*2));
			else innerRectShape.setSize(Vector2f(l-border*2, s.Width-border*2));

			innerRectShape.setPosition(min(p1.x, p2.x)-s.Width/2+x+border, min(p1.y, p2.y)-s.Width/2+y+border);

			if (s.State == Snake::FADING)
			{
				innerRectShape.setFillColor(Game::mapColor);
				Window->draw(innerRectShape);
				int alpha =  (1.0f - (float)(GetTickCount() - s.GetStateChangeTime()) / (float)Snake::FadingTime) * 255;
				innerRectShape.setFillColor(Color(70, 70, 70, alpha));
			}
			else
			{
				innerRectShape.setFillColor(s.Col);
			}


			Window->draw(innerRectShape);
		}
	}
}

void Game::Draw(int x, int y)
{
	int u = x+10;
	int v = y+10;

	Window->clear(APPLICATION::Application::clearColor);

	DrawMap(u, v);
	DrawSnakes(u, v);
	DrawNotifications(u, v);

	if (State == SCORES)
		DrawScores(u, v);
}

void Game::DrawScores(int x, int y)
{
	Window->draw(ScoresRect);
	text.setString(ScoresString);
	text.setPosition(ScoresRect.getPosition().x+10, ScoresRect.getPosition().y+10);
	Window->draw(text);
}


APPLICATION::ApplicationStage* Game::OnUpdate(float dt)
{
	if (State == CLOSE) return Parent;

	for (int i=0;i<Snakes.size();i++)
	{
		if (!Snakes[i].IsDead())
			Snakes[i].Update(dt);
	}

	for (int i=0;i<Notifications.size();i++)
	{
		if (GetTickCount() - Notifications[i].StartTime >= NotificationTime)
		{
			Notifications.erase(Notifications.begin()+i);
			i--;
		}
		else
		{
			Notifications[i].Pos += Vec2(0.0f, -50.0f)*dt;
		}
	}



	MapObj.Update(dt);
	//CheckCollisions();

	if (GetAliveSnakesNum() == 0)
	{
		BuildScores();
		State = SCORES;
	}

	return NULL;
}

int Game::GetAliveSnakesNum()
{
	int alive = 0;
	for (int i=0;i<Snakes.size();i++)
	{
		if (!Snakes[i].IsDead()) alive++;
	}
	return alive;
}

void Game::Release()
{
	ArrowController.StopListenKeyboard();
	WSADController.StopListenKeyboard();

	End();
}

int Game::RandomBonus()
{
	return rand()%(Bonus::BONUSCOUNT-1)+1;
	//return Bonus::ARMOR;
}

void Game::OnKeyDown(sf::Keyboard::Key key)
{
	if (key == Keyboard::Return && State == SCORES)
	{
		State = CLOSE;
	}
}

void Game::Block()
{
	BASE::Thread::Wait(GameMutex);
}

void Game::Unblock()
{
	BASE::Thread::Release(GameMutex);
}

BASE::DataBuffer* BASE::Serialization::SerializeScores(vector<Score>& scores)
{
	int size = 0;
	for (int i=0;i<scores.size();i++)
	{
		size += scores[i].Name.size();
	}
	size += scores.size()*8;
	size += 4;

	char* buff = new char[size];

	int pos = 0;
	buff[pos] = scores.size();
	pos += sizeof(int);
	for (int i=0;i<scores.size();i++)
	{
		buff[pos] = scores[i].Name.size();
		pos += sizeof(int);
		for (int j=0;j<scores[i].Name.size();j++)
		{
			buff[pos++] = scores[i].Name[j];
		}
		memcpy(buff+pos,  (void*)&scores[i].Value, sizeof(int));

		pos += sizeof(int);
	}

	return new BASE::DataBuffer(buff, size);
}

void BASE::Serialization::DeserializeScores(char* data, vector<Score>& scores)
{
	scores.clear();

	int pos = 0;
	int count = data[pos];
	pos += sizeof(int);
	scores.clear();
	scores.resize(count);

	for (int i=0;i<count;i++)
	{
		int length = data[pos];
		pos += sizeof(int);
		for (int j=0;j<length;j++)
		{
			scores[i].Name.push_back(data[pos++]);
		}
		memcpy(&scores[i].Value,  data+pos, sizeof(int));
		pos += sizeof(int);
	}
}