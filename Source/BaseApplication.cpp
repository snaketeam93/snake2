#include"Base.h"
#include"Timer.h"
#include"ApplicationStage.h"
#include "BaseApplication.h"
#include<sstream>

using namespace std;
using namespace sf;
using namespace APPLICATION;
using namespace BASE;


void BaseApplication::Init(int Width, int Height, const string& title)
{
	AppWindow.create(sf::VideoMode(Width, Height), title);

	ApplicationStage::WindowRect = MATH::Rect(0, 0, Width, Height);
	//DrawableObject::SetWindow(&AppWindow);

	OnInit();
}

void BaseApplication::UpdateEvents()
{
	Event event;
	static bool shift = false, alt = false;

	while (AppWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			AppWindow.close();

		if (event.type == Event::KeyPressed)
		{
			if (event.key.code == Keyboard::LShift || event.key.code == Keyboard::RShift) shift = true;
			if (event.key.code == Keyboard::LAlt || event.key.code == Keyboard::RAlt) alt = true;

			for (int i=0;i<KeyboardListener::GetListenersCount();i++)
			{
				if (KeyboardListener::GetListener(i)->isActive()) KeyboardListener::GetListener(i)->KeyDown(event.key.code, shift, alt);
			}
		}

		if (event.type == Event::KeyReleased)
		{
			if (event.key.code == Keyboard::LShift || event.key.code == Keyboard::RShift) shift = false;
			if (event.key.code == Keyboard::LAlt || event.key.code == Keyboard::RAlt) alt = false;

			for (int i=0;i<KeyboardListener::GetListenersCount();i++)
			{
				if (KeyboardListener::GetListener(i)->isActive()) KeyboardListener::GetListener(i)->KeyUp(event.key.code);
			}
		}
	}
}

void BaseApplication::Run()
{
	static Timer FrameTimer;
	//static float WorldTime;
	FrameTimer.Start();

	while (AppWindow.isOpen())
	{
		FrameTimer.Update();

		

		UpdateEvents();
		AppWindow.clear();

		if (ActiveStage)
		{
			ApplicationStage* stage = ActiveStage->Update(FrameTimer.GetDt());
			if (stage != NULL) SetActiveStage(stage);
			else ActiveStage->Draw(0, 0);
		}
		OnUpdate(FrameTimer.GetDt());

		AppWindow.display();

		AppWindow.setTitle(FloatToString(FrameTimer.GetFPS()));
	}

	if (ActiveStage) ActiveStage->Close();
}

void BaseApplication::Release()
{
	OnRelease();
}

void BaseApplication::SetActiveStage(ApplicationStage* stage)
{
	if (!stage) return;
	ApplicationStage* prevStage = NULL;
	if (ActiveStage) 
	{
		prevStage = ActiveStage;
		ActiveStage->Close();
	}

	ActiveStage = stage;
	ActiveStage->Open();

	OnStageChange(prevStage, ActiveStage);
}

