#pragma once
#include<vector>
#include"Events.h"

namespace GAME
{
	class GameEventListener
	{
		friend class Game;
	protected:
		static std::vector<GameEventListener*> Listeners;
	public:
		GameEventListener() { Listeners.push_back(this); }

		virtual void OnReceiveGameEvent(GameEvent gevent) = 0;
		virtual ~GameEventListener() {}
	};
};