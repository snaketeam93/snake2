﻿#pragma once
//#define WIN32_LEAN_AND_MEAN
//#include<Windows.h>

namespace NETWORK
{
	void InitNetwork();
	void ReleaseNetwork();
	u_long ResolveHost( const std::string &host );
	
	
	class SocketListener
	{
	protected:
		bool Active;
	public:
		SocketListener() : Active(false) {}
		void StartListenSocket() { Active = true; }
		void StopListenSocket() { Active = false; }
		bool IsActive() const { return Active; }

		virtual void OnReceive(char* buffer, int size) = 0;
		virtual void OnSend(char* data, int size) = 0;
		virtual ~SocketListener() {}
	};

	class Socket
	{
	protected:
		SOCKET socketHandle;
		SOCKADDR_IN saddr;

		std::vector<SocketListener*> Listeners;
	public:
		Socket();
		Socket(SOCKET sock);
		virtual ~Socket() {}

		bool Connect(const std::string& host, int port=27015);
		virtual bool Connect(u_long host, int port=27015);
		void Send(char* data, int size);
		void Receive(char* buffer, int size);
		void Close();

		void AddListener(SocketListener* listener);
	};

	class ServerSocket : public Socket
	{
	public:
		bool Connect(u_long host);

		void Init(int port);
		Socket* GetClient();
	};
};