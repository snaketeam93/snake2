#pragma once
#include"Base.h"
#include"ApplicationStage.h"
#include"Map.h"
#include"Snake.h"
#include"Events.h"
#include"GameEventLinstener.h"
#include"Controller.h"

const int PORT1 =  50015;
const int PORT2 =  50016;
const std::string SERVER = "localhost";

namespace GAME
{
	struct StartState
	{
		MATH::Vec2 Pos;
		MATH::Vec2 Dir;
	};

	struct Window
	{
		static const int Width = 950;
		static const int Height = 650;
	};


	class Notification
	{
	public:
		MATH::Vec2 Pos;
		std::string Text;
		int StartTime;

		Notification(const MATH::Vec2& p, const std::string& str, int st) : Pos(p), Text(str), StartTime(st) {}
	};

	struct Score
	{
		std::string Name;
		int Value;

		Score() {}
		Score(const std::string& n, int v) : Name(n), Value(v) {}

		static bool Compare(const Score& s1, const Score& s2)
		{
			return s2.Value < s1.Value;
		}
	};

	class Game : public APPLICATION::ApplicationStage
	{
	private:
		//static const int ObjectsCount = 30;
		static const int FoodScore = 10;

		sf::RectangleShape mapRect;
		sf::RectangleShape ScoresRect;
		sf::RectangleShape UIrect;
		sf::RectangleShape bar;
		sf::RectangleShape innerBar;
		sf::RectangleShape rectShape;
		sf::RectangleShape innerRectShape;
		sf::CircleShape objCircle; 
		sf::Text text;


		static const int NotificationTime = 1000;
		
		enum STATE { GAME, SCORES, CLOSE };
		STATE State;
		std::vector<Score> Scores;
		std::string ScoresString;

		int Margin;
		int MapW;
		int MapH;

		bool bEnd;
		bool Blocked;

		static StartState StartPoints[4];
		sf::Color SnakesColors[4];

		GAME::LocalController ArrowController, WSADController;

		sf::Font GameFont;

		Map MapObj;
		std::vector<Snake> Snakes;
		std::vector<Notification> Notifications;

		int PlayersCount;

		void DrawMap(int x, int y);
		void DrawSnakes(int x, int y);
		void DrawSnakeUI(int snake, int x, int y);
		void DrawNotifications(int x, int y);
		void DrawScores(int x, int y);

		void BuildScores();

		HANDLE GameMutex;

		void SendScores();
	public:
		static const sf::Color mapColor;
		static const sf::Color foodColor;
		static const sf::Color bonusColor;
		static const sf::Color trapColor;
		static const sf::Color blackColor;
		

		static const int ObjectsCount = 15;

		Game() : bEnd(true) {}

		void OnEscape() 
		{ 
			ArrowController.StopListenKeyboard();
			WSADController.StopListenKeyboard();
		}

		void Block();
		void Unblock();

		void Init() {}
		void Release();
		void OnOpen()
		{ 
			ArrowController.StartListenKeyboard();
			WSADController.StartListenKeyboard();

			Escape = false; 
			Unblock();
		}

		LocalController& GetArrowController() { return ArrowController; }
		LocalController& GetWSADController() { return WSADController; }

		ApplicationStage* OnUpdate(float dt);
		void OnKeyDown(sf::Keyboard::Key key);
		void Draw(int x, int y);

		std::vector<Score>& GetScores() { return Scores; }
		void ScoresToString(std::vector<Score>& scores, std::string& output);
		void End();
		void BaseInit();
		void Update(GameEvent ge);
		void CheckCollisions();
		
		bool Exist() { return !bEnd; }
		int RandomBonus();

		void Create(int numPlayers, int maxFood, std::string* names);

		int GetPlayersCount() const { return PlayersCount; }

		int GetAliveSnakesNum();
		Snake& GetSnake(int i) { return Snakes[i]; }
		Map& GetMap() { return MapObj; }

		void PostEvent(GameEvent gameEvent);
		
	};
};

namespace BASE
{
	namespace Serialization
	{
		void DeserializeScores(char* data, std::vector<GAME::Score>& scores);
		DataBuffer* SerializeScores(std::vector<GAME::Score>& scores);
	};
};
