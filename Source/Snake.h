#pragma once
#include<list>
#include"Math.h"
#include<SFML\Graphics.hpp>
#include"GameObject.h"


namespace GAME
{
	struct ControlPoint
	{
		MATH::Vec2 Pos;
		MATH::Vec2 Dir;
		float Length;

		ControlPoint() {}
		ControlPoint(MATH::Vec2 p, MATH::Vec2 d, float l) : Pos(p), Dir(d), Length(l) {}
	};

	class Bonus
	{
	public:
		enum TYPE { NONE = 0, SPEEDUP, SPEEDDOWN, REVERSE, FOODX2, ARMOR, BONUSCOUNT };

		static std::string Names[BONUSCOUNT];
	};



	class Snake : public GameObject
	{
		friend class Game;
	public:
		enum STATE { ALIVE, FADING, DEAD };
	private:
		static const int DefaultSpeed = 80;
		static const int BonusTime = 8000; 
		static const int Width = 10;
		static const int FadingTime = 3000; 

		//std::list<ControlPoint> Points;
		std::vector<ControlPoint> Points;

		int Lives;
		int Score;
		std::string Name;

		//std::list<sf::Vector2i> Elements;
		
		float Speed;
		sf::Color Col;
		float TimeStep;
		float Length;
		bool Moved;
		bool tailMove;
		int Index;

		Bonus::TYPE CurrentBonus;
		int BonusStartTime;
		int ScoreFactor;
		bool Armor;
		std::string PlayerName;
		
		STATE State;
		int StateChangeTime;

		//MATH::Rect CreateSegment(const ControlPoint& cp1, const ControlPoint& cp2);
		void UpdateSegment(int i);

	public:
		int GetStateChangeTime() const { return StateChangeTime; }

		void SetState(STATE s) { State = s; StateChangeTime = GetTickCount(); }
		bool IsAlive() const { return State == ALIVE; }
		bool IsDead() const { return State == DEAD; }

		void Move(int dx, int dy, bool report=true);
		void Update(double dt);
		void Draw(int x, int y);

		ControlPoint& GetPoint(int i);
		static bool ValidPoint(ControlPoint& point);

		const ControlPoint& GetHead() { return GetPoint(0); }//Points[0]; }
		const ControlPoint& GetTail() { return GetPoint(Points.size()-1); }//Points[Points.size()-1]; }

		bool Collide(const MATH::Vec2& pos, int w, int h, MATH::Vec2& pushVec);
		bool AutoCollide();

		void Sync(const MATH::Vec2& head);
		
		void AddElementFront(ControlPoint v) ;
		void AddElementBack(ControlPoint v);
		void EraseBack();

		void Clear();

		void SetColor(sf::Color c)		{ Col = c;					}
		void SetLength(float l)			{ Length = l;				}
		void AddLength(int l)			{ Points.back().Length += l;}
		int  GetLength(int el=-1);
		void UpdateLength();

		void StartBonus(Bonus::TYPE bonus);
		void StopBonus();

		void SetPlayerName(const std::string& name) { PlayerName = name; }

		Snake(void) { Clear(); }
		~Snake(void) {}
	};

};
