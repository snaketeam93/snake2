#pragma once
#include"..\Source\Base.h"
#include"..\Source\Controller.h"
#include"..\Source\Game.h"
#include"..\Source\Network.h"
#include"..\Source\GameEventLinstener.h"
//#include<thread>

namespace CLIENT
{
	class Client;
	class ServerListener
	{
	protected:
		Client* Cl;
		NETWORK::Socket* pServer;
	public:
		ServerListener(Client* cl) : Cl(cl) {}

		void Start(NETWORK::Socket* server)
		{
			pServer = server;	
			OnStart();
		}

		virtual void OnStart()  {};
		virtual ~ServerListener() {}
	};


	class ServerDataListener : public ServerListener, public BASE::Runnable
	{
	public: 
		ServerDataListener(Client* cl) : ServerListener(cl) {}
		
		void OnStart() 
		{
			BASE::Thread::NewThread(this);
		}
		int operator()();
		void Run() { operator()(); }
	};

	class ServerEventListener : public ServerListener, public BASE::Runnable
	{
	public: 
		ServerEventListener(Client* cl) : ServerListener(cl) {}

		void OnStart() 
		{ 
			BASE::Thread::NewThread(this);
		}
		int operator()();
		void Run() { operator()(); }
	};

	class Client : public GAME::GameEventListener
	{
	private:
		int Index;
		int ServerTime;
		int TimeOffset;

		

		ServerEventListener* ServEventListener;
		ServerDataListener* ServDataListener;

		NETWORK::Socket Server;
		NETWORK::Socket ServerData;


		std::string Name;
		GAME::Game* SnakeGame;
		float FPS;
		bool bRun;

		void UpdateGame();

		void Update(float dt);
		void Draw();

		std::vector<GAME::GameEvent> EventsQueue;
		HANDLE EventQueueMutex;

		std::vector<GAME::Score> Scores;
		std::vector<GAME::Score> GameScores;
		std::string ScoresString;
		
	public:
		Client() : ServEventListener(0), ServDataListener(0), ScoresString("") {}

		void AddEventToQueue(GAME::GameEvent ev);
		void OnReceiveEventFromServer(GAME::GameEvent ge);
		void ProcessEvents();
		const std::string& GetScoresString() { return ScoresString; }

		void OnReceiveGameEvent(GAME::GameEvent ge);
		void StartGameRequest();
		void HighScoresRequest();
		void SendScores();
		void SaveGameScores() { GameScores = SnakeGame->GetScores(); }

		bool Running() const { return bRun; }
		bool Init(GAME::Game& game, const std::string& server, const std::string& clientname);
		void Run();
		void Release();
	};
};
