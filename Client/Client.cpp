#include"Client.h"
#include"..\Source\Timer.h"
#include<sstream>
#include<list>

using namespace NETWORK;
using namespace CLIENT;
using namespace BASE;
using namespace GAME;
using namespace sf;
using namespace std;


bool Client::Init(Game& game, const string& server, const string& name)
{
	NETWORK::InitNetwork();
	bRun = true;

	ServEventListener = new ServerEventListener(this);
	ServDataListener = new ServerDataListener(this);

	Name = name;
	SnakeGame = &game;

	//event socket
	if (!Server.Connect(SERVER, PORT1)) 
	{
		MessageBox(0, "Nie uda�o si� po��czy� (event socket)", "error", 0);
		return false;
	}

	//data socket
	if (!ServerData.Connect(SERVER, PORT2)) 
	{
		MessageBox(0, "Nie uda�o si� po��czy� (data socket)", "error", 0);
		return false;
	}

	ServEventListener->Start(&Server);	
	ServDataListener->Start(&ServerData);	

	return true;
}

/*
void Client::Update(float dt)
{
SnakeGame.Update(dt);
}

void Client::Draw()
{
WindowApp.clear();
SnakeGame.Draw();
WindowApp.display();
}

void Client::Run()
{
static Timer FrameTimer;
//static float WorldTime;
FrameTimer.Start();

while (WindowApp.isOpen() && bRun)
{
FrameTimer.Update();
Update(FrameTimer.GetDt());
Draw();

//if (Index == 0) _sleep(100);

WindowApp.setTitle(FloatToString(FrameTimer.GetFPS()));
}
bRun = false;
SnakeGame.End();
}*/

void Client::Release()
{
	//ServEventListener->Release();
	//ServDataListener->Release();
	bRun = false;
	if (ServEventListener)
	{
		Server.Send((char*)&GameEvent(GameEvent::CLOSECONNECTION), sizeof(GameEvent));

		ServEventListener->Join();
		
		delete ServEventListener;
		ServEventListener = NULL;

		Server.Close();
	}

	if (ServDataListener)
	{
		ServerData.Send((char*)&GameEvent(GameEvent::CLOSECONNECTION), sizeof(GameEvent));

		ServDataListener->Join();
		
		delete ServDataListener;
		ServDataListener = NULL;

		ServerData.Close();
	}


	NETWORK::ReleaseNetwork();
}


void Client::OnReceiveEventFromServer(GameEvent ge)
{
	if (ge.Type == GameEvent::NONE) return;

	if (ge.Type == GameEvent::CLOSECONNECTION)
	{
		bRun = false;
	}
	if (ge.Type == GameEvent::STARTGAME)
	{
		SnakeGame->Create(ge.StartGame.NumPlayers, ge.StartGame.MaxObjects, ge.StartGame.Names);
		SnakeGame->GetArrowController().AddSnake(SnakeGame->GetSnake(Index));
		SnakeGame->GetWSADController().AddSnake(SnakeGame->GetSnake(Index));

		ServerTime = ge.StartGame.Time;

		TimeOffset = ServerTime - GetTickCount();

		delete[] ge.StartGame.Names;
	}

	if (ge.Type == GameEvent::SCORESPACK)
	{
		char* buffer = new char[ge.ScoresPack.Size];
		Server.Receive(buffer, ge.ScoresPack.Size);
		Serialization::DeserializeScores(buffer, Scores);
		SnakeGame->ScoresToString(Scores, ScoresString);
		delete[] buffer;
	}

	//if (!SnakeGame.Exist()) return;

	else if (ge.Type == GameEvent::CLIENTINDEX)
	{
		Index = ge.ClientIndex.ClientIndex;

		GameEvent e = GameEvent(GameEvent::CLIENTNAME, Index, Name.size());
		Server.Send((char*)&e, sizeof(GameEvent));
		Server.Send((char*)Name.c_str(), Name.size());
	}
	else if (ge.Type == GameEvent::SYNCHOBJECTS)
	{
		ge.SyncObjects.SyncTime = GetTickCount() - ge.SyncObjects.SyncTime + TimeOffset; //miliseconds
	}

	if (SnakeGame->Exist()) SnakeGame->Update(ge);
	else if (ge.Type == GameEvent::SYNCHOBJECTS) delete[] ge.SyncObjects.Data;
}

void Client::OnReceiveGameEvent(GAME::GameEvent ge)
{
	if (ge.Type == GameEvent::CHANGEDIR || 
		ge.Type == GameEvent::CLOSEGAME ||
		ge.Type == GameEvent::ONSCORE ||
		ge.Type == GameEvent::ONBONUS)
	{
		Server.Send((char*)&ge, sizeof(GameEvent));
	}

	if (ge.Type == GameEvent::SCORESPACK)
	{
		//BASE::DataBuffer* buffer = BASE::Serialization::SerializeStrings(ge.ScoresPack.ScoresString, 1);
		//Server.Send((char*)&ge, sizeof(GameEvent));
		//Server.Send(buffer->Data, buffer->Size);
	}
}


int ServerDataListener::operator()()
{
	static GameEvent ge;
	while (1)
	{
		if (!Cl->Running()) break;

		//_sleep(10);

		pServer->Receive((char*)&ge, sizeof(GameEvent));

		if (ge.Type == GameEvent::CLOSECONNECTION || ge.Type == GameEvent::NONE) break;

		if (ge.Type == GameEvent::SYNCHOBJECTS)
		{

			int size = ge.SyncObjects.Objects*4;
			float* objectsData = new float[size];

			pServer->Receive((char*)objectsData, sizeof(float)*size);

			ge.SyncObjects.Data = objectsData;
		}

		Cl->OnReceiveEventFromServer(ge);
		//Cl->AddEventToQueue(ge);
	}

	return 0;
}

int ServerEventListener::operator()()
{
	static GameEvent ge;
	while (1)
	{
		//_sleep(10);
		if (!Cl->Running()) break;

		pServer->Receive((char*)&ge, sizeof(GameEvent));

		if (ge.Type == GameEvent::STARTGAME)
		{
			char* namesData = new char[(int)ge.StartGame.Names];
			pServer->Receive(namesData, (int)ge.StartGame.Names);

			string* names = new string[ge.StartGame.NumPlayers];
			Serialization::DeserializeStrings(names, DataBuffer(namesData, 0));
			ge.StartGame.Names = names;
			delete[] namesData;
		}

		if (ge.Valid() && ge.Type!= GameEvent::SYNCHOBJECTS)
			//Cl->AddEventToQueue(ge);
			Cl->OnReceiveEventFromServer(ge);

		if (ge.Type == GameEvent::CLOSECONNECTION || ge.Type == GameEvent::NONE) break;
	}

	return 0;
}

void Client::AddEventToQueue(GameEvent ev)
{
	BASE::Thread::Wait(EventQueueMutex);
	
	EventsQueue.push_back(ev);

	BASE::Thread::Release(EventQueueMutex);
}

void Client::StartGameRequest()
{
	GameEvent ge = GameEvent(GameEvent::STARTGAMEREQUEST);
	Server.Send((char*)&ge, sizeof(GameEvent));
}

void Client::SendScores()
{
	if (GameScores.size() == 0) return;

	//vector<Score>& scores = SnakeGame->GetScores();
	DataBuffer* buffer = Serialization::SerializeScores(GameScores);

	GameEvent ge = GameEvent(GameEvent::SCORESPACK, Index, buffer->Size);
	Server.Send((char*)&ge, sizeof(GameEvent));
	Server.Send(buffer->Data, buffer->Size);

	buffer->Release();
	delete buffer;

	GameScores.clear();
}

void Client::HighScoresRequest()
{
	GameEvent ge = GameEvent(GameEvent::HIGHSCORESREQUEST, Index);
	Server.Send((char*)&ge, sizeof(GameEvent));
}

void Client::ProcessEvents()
{
	BASE::Thread::Wait(EventQueueMutex);
	

	for (int i=0;i<EventsQueue.size();i++)
	{
		OnReceiveEventFromServer(EventsQueue[i]);
	}
	EventsQueue.clear();

	BASE::Thread::Release(EventQueueMutex);
}