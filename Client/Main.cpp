#include"..\Source\Application.h"
#include<Windows.h>

using namespace APPLICATION;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR Cmd, int numCmd)
{
	
	#if defined _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(0x011C916F);
	#endif // _DEBUG
	////////////*/


	Application App;
	App.Init(GAME::Window::Width, GAME::Window::Height, "Snake");
	App.Run();
	App.Release();

	return 0;
}