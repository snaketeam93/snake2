// Server.cpp : Defines the entry point for the console application.
//

#include"..\Source\Base.h"
#include"Server.h"
#include"..\Source\Timer.h"
#include<iostream>
#include<fstream>
#include<algorithm>

using namespace NETWORK;
using namespace BASE;
using namespace GAME;
using namespace std;

//const sf::Color Application::clearColor = sf::Color(52, 133, 167, 255);

string GameEvent::TypeStr[GameEvent::NUMEVENTS] = { "NONE", "STARTGAME", "CLIENTINDEX", "CHANGEDIR", "SYNCHOBJECTS", "NEWOBJECT", "CLOSEGAME", "ONSCORE", "ONBONUS", "SNAKEDEATH", 
						"STARTGAMEREQUEST", "HIGHSCORESREQUEST", "CLIENTNAME", "CLOSECONNECTION" };

void GameProcess::operator()()
{
	Timer FrameTimer;
	FrameTimer.Start();


	static float SynchTime = 0.0f;
	static float SynchInterval = 1.0f;
	static float LastSynch = 1.0f;

	pServer->PrintLine("GameProcess zaczyna prace");
	while (pGame->Exist())
	{
		//pGame->Block();

		FrameTimer.Update();
		pGame->OnUpdate(FrameTimer.GetDt());
		pGame->CheckCollisions();

		SynchTime = FrameTimer.GetSecs();

		if (SynchTime - LastSynch >= SynchInterval && pGame->GetAliveSnakesNum() > 0)
		{
			pServer->SynchronizeObjects();
			LastSynch = FrameTimer.GetSecs();	
		}	

		//pGame->Unblock();
	}
	pServer->PrintLine("GameProcess konczy prace");
	pGame->Release();
}

void ClientListener::operator()()
{
	GameEvent gameEvent;

	Serv->PrintLine("ClientListenerProcess zaczyna prace");
	while(Serv->Running())
	{
		//_sleep(10);

		pClient->Receive((char*)&gameEvent, sizeof(GameEvent));

		Serv->PrintLine("\tEvent: [ "+gameEvent.ToString()+" ]\n");

		if (gameEvent.Type == GameEvent::CLIENTNAME)
		{
			char* nameData = new char[gameEvent.ClientName.NameLength+1];
			pClient->Receive(nameData, gameEvent.ClientName.NameLength);
			nameData[gameEvent.ClientName.NameLength] = 0;
			string* name = new string(nameData);
			gameEvent.ClientName.pName = name;

			delete[] nameData;
		}
		//Serv->AddEventToQueue(gameEvent, Index);
		Serv->OnReceiveGameEventFromClient(Index, gameEvent);	

		if (gameEvent.Type == GameEvent::CLOSECONNECTION) break;
	}
	Serv->PrintLine("ClientListenerProcess konczy prace");
}

void Server::SynchronizeObjects()
{
	if (!Synchronization) return;

	PrintLine("\tsync");

	int objects = SnakeGame.GetMap().GetMapObjectsCount();

	GameEvent ge = GameEvent(GameEvent::SYNCHOBJECTS, objects, GetTickCount());
	for (int i=0;i<Clients.size();i++)
	{
		ClientsData[i]->Send((char*)&ge, sizeof(GameEvent));
	}

	//ka�dy obiekt
	//float (x) float (y)  float (dx) float (dy)


	float* objectsData = new float[objects*4];

	int k = 0;
	for (int i=0;i<objects;i++)
	{
		const MapObject* fo = &SnakeGame.GetMap().GetFood(i);

		objectsData[k++] = fo->GetPos().x;
		objectsData[k++] = fo->GetPos().y;
		objectsData[k++] = fo->GetV().x;
		objectsData[k++] = fo->GetV().y;
	}

	for (int i=0;i<Clients.size();i++)
	{
		ClientsData[i]->Send((char*)objectsData, sizeof(float)*objects*4);
	}

	delete[] objectsData;
}

void Server::BroadcastEvent(GameEvent ge)
{
	for (int i=0;i<Clients.size();i++)
	{
		Clients[i]->Send((char*)&ge, sizeof(GameEvent));
	}
}

void Server::Run(int players)
{
	GameRequests = 0;
	EndGameRequests = 0;
	numClients = players;
	bRun = true;
	Serv.Init(PORT1);
	DataServ.Init(PORT2);
	LoadScores("scores.txt");
	
	for (int i=0;i<players;i++)
	{
		Clients.push_back(Serv.GetClient());
		ClientsData.push_back(DataServ.GetClient());

		ClientListener* cl = new ClientListener(i, this);
		cl->Start(Clients[i]);
		ClientListeners.push_back(cl);

		_sleep(10);

		GameEvent ge = GameEvent(GameEvent::CLIENTINDEX, i);
		Clients[i]->Send((char*)&ge, sizeof(GameEvent));
	}

	//ServerProc = new ServerProcess(this);
	//ServerProc->Start();

	Serv.Close();
	DataServ.Close();

	//int i;
	//cin >> i;
}

void GameProcess::Start(GAME::Game* game)
{
	pGame = game;
	Thread::NewThread(this);
	//pThread = new thread(*this);
}

void ClientListener::Start(NETWORK::Socket* sock)
{
	pClient = sock;
	//pThread = new thread(*this);
	Thread::NewThread(this);
}

void ServerProcess::Start()
{
	Thread::NewThread(this);
}

void GameProcess::Release()
{
	//pThread->join();
	//delete pThread;
}

void ClientListener::Release()
{
	//delete pThread;
}

void Server::Release()
{
	bRun = false;

	//ServerProc->Join();
	//delete ServerProc;
	//ServerProc = NULL;

	BroadcastEvent(GameEvent(GameEvent::CLOSECONNECTION));
	SnakeGame.Release();

	if (GameProc)
	{
		GameProc->Join();
		GameProc->Release();
		delete GameProc;
	}

	for (int i=0;i<Clients.size();i++)
	{
		Clients[i]->Close();
		ClientsData[i]->Close();

		delete Clients[i];
		delete ClientsData[i];
	}

	for (int i=0;i<ClientListeners.size();i++)
	{
		ClientListeners[i]->Join();
		ClientListeners[i]->Release();
		delete ClientListeners[i];
	}
}

int main()
{
#if defined _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(256);
#endif // _DEBUG

	NETWORK::InitNetwork();
	Server Serv;

	string Cmd;

	while(1)
	{
		cin >> Cmd;

		if (Cmd == "exit") break;
		if (Cmd == "start")
		{
			int players = 1;
			cin >> players;

			Serv.Run(players);
		}
	}


	Serv.Release();

	return 0;
}

void ServerProcess::Run()
{
	while (pServer->Running())
	{
		pServer->ProcessEvents();
	}	
}

void Server::PrintLine(const std::string& msg)
{
	cout << msg << "\n";
}

void Server::OnReceiveGameEventFromClient(int clientIndex, GameEvent ge)
{
	static int scoresPack = 0;

	if (ge.Type == GameEvent::CLIENTNAME)
	{
		ClientNames[ge.ClientName.ClientIndex] = *ge.ClientName.pName;

		PrintLine("Dolaczyl " + *ge.ClientName.pName);
		delete ge.ClientName.pName;
		return;
	}
	if (ge.Type == GameEvent::STARTGAMEREQUEST)
	{
		GameRequests++;
		PrintLine("Zadanie gry");

		if (GameRequests == numClients)
		{
			scoresPack = 0;
			PrintLine("Gra zaczeta");
			GameRequests = 0;
			DataBuffer* buffer = Serialization::SerializeStrings(ClientNames, numClients);

			GameEvent ge = GameEvent(GameEvent::STARTGAME, numClients, Game::ObjectsCount, 0, (string*)buffer->Size);
			BroadcastEvent(ge);

			for (int i=0;i<Clients.size();i++)
			{
				Clients[i]->Send(buffer->Data, buffer->Size);
			}
			buffer->Release();
			delete buffer;

			APPLICATION::ApplicationStage::SetWindowRect(MATH::Rect(0, 0, Window::Width, Window::Height));
			SnakeGame.BaseInit();
			SnakeGame.Create(numClients, Game::ObjectsCount, ClientNames);
			SnakeGame.GetMap().GenerateObjects();
			GameProc = new GameProcess(this);
			GameProc->Start(&SnakeGame);
			SynchronizeObjects();

			return;
		}
	}

	if (ge.Type == GameEvent::CLOSEGAME)
	{
		EndGameRequests++;
		if (EndGameRequests == numClients)
		{
			EndGameRequests = 0;
			PrintLine("Gra skonczona");
			SnakeGame.Release();
			GameProc->Join();
			GameProc->Release();
			delete GameProc;
			GameProc = NULL;
		}
		return;
	}

	if (ge.Type == GameEvent::CLOSECONNECTION && clientIndex < Clients.size())
	{
		PrintLine(ClientNames[clientIndex] + "odlaczyl sie");

		Clients[clientIndex]->Send((char*)&ge, sizeof(GameEvent));
		ClientsData[clientIndex]->Send((char*)&ge, sizeof(GameEvent));

		Clients[clientIndex]->Close();
		ClientsData[clientIndex]->Close();
		
		delete ClientListeners[clientIndex];
		ClientListeners.erase(ClientListeners.begin()+clientIndex);

		delete Clients[clientIndex];
		delete ClientsData[clientIndex];

		Clients.erase(Clients.begin()+clientIndex);
		ClientsData.erase(ClientsData.begin()+clientIndex);

		return;
	}

	if (ge.Type == GameEvent::HIGHSCORESREQUEST)
	{
		DataBuffer* buffer = Serialization::SerializeScores(Scores);

		GameEvent ev = GameEvent(GameEvent::SCORESPACK, 0, buffer->Size);

		Clients[ge.HighScoresRequest.ClientIndex]->Send((char*)&ev, sizeof(GameEvent));
		Clients[ge.HighScoresRequest.ClientIndex]->Send(buffer->Data, buffer->Size);

		buffer->Release();
		delete buffer;

		return;
	}

	if (ge.Type == GameEvent::SCORESPACK)
	{
		
		char* buffer = new char[ge.ScoresPack.Size];
		Clients[ge.ScoresPack.ClientIndex]->Receive(buffer, ge.ScoresPack.Size);
		
		if (scoresPack > 0) 
		{
			delete[] buffer;
			return;
		}

		vector<Score> scoresTmp;
		Serialization::DeserializeScores(buffer, scoresTmp);
		
		for (int i=0;i<scoresTmp.size();i++)
		{
			Scores.push_back(scoresTmp[i]);
		}

		sort(Scores.begin(), Scores.end(), Score::Compare);
		if (Scores.size() > 5)
		{
			Scores.erase(Scores.begin()+5, Scores.end());
		}
		SaveScores("scores.txt");

		delete[] buffer;
		scoresPack ++;
		return;
	}

	for (int i=0;i<Clients.size();i++)
	{
		if (i != clientIndex)
		{
			Clients[i]->Send((char*)&ge, sizeof(GameEvent));
		}
	}

	if (SnakeGame.Exist()) SnakeGame.Update(ge);
}

void Server::OnReceiveGameEvent(GameEvent gevent)
{
	if (gevent.Type == GameEvent::CLOSEGAME) return;

	if (gevent.Type == GameEvent::STARTGAME)
	{
		gevent.StartGame.Time = GetTickCount();
	}

	for (int i=0;i<Clients.size();i++)
	{
		Clients[i]->Send((char*)&gevent, sizeof(GameEvent));
	}
}

void Server::AddEventToQueue(GameEvent ev, int client)
{
	BASE::Thread::Wait(EventQueueMutex[client]);
	
	EventsQueue[client].push_back(ev);

	BASE::Thread::Release(EventQueueMutex[client]);
}

void Server::LoadScores(const string& path)
{
	Scores.clear();
	ifstream file(path);
	string name, val;

	while (getline(file, name))
	{
		getline(file, val);
		Scores.push_back(Score(name, atoi(val.c_str())));
	}

	file.close();
}

void Server::SaveScores(const string& path)
{
	ofstream file(path);

	char buff[5];
	for (int i=0;i<Scores.size();i++)
	{
		file << Scores[i].Name;
		file << endl;
		itoa(Scores[i].Value, buff, 10);
		file << buff;
		file << endl;
	}

	file.close();
}

void Server::ProcessEvents()
{
	for (int i=0;i<numClients;i++)
	{
		BASE::Thread::Wait(EventQueueMutex[i]);
	
		for (int j=0;j<EventsQueue[i].size();j++)
		{
			SnakeGame.Block();
			OnReceiveGameEventFromClient(i, EventsQueue[i][j]);
			SnakeGame.Unblock();
		}
		EventsQueue[i].clear();

		BASE::Thread::Release(EventQueueMutex[i]);
	}
}