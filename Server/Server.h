#pragma once

#include"..\Source\Network.h"
#include"..\Source\Game.h"
#include"..\Source\GameEventLinstener.h"
#include<vector>
#include<list>
//////////#include<thread>  

class GameProcess;
class ClientListener;

class Server;
class ServerProcess : public BASE::Runnable
{
private :
	Server* pServer;
public:
	ServerProcess(Server* serv) : pServer(serv) {}

	void Run();
	void Start();
};

class Server : public GAME::GameEventListener
{
	static const bool Synchronization = true;
	static const int MaxPlayers = 4;

	NETWORK::ServerSocket Serv;
	NETWORK::ServerSocket DataServ;

	GAME::Game SnakeGame;
	GameProcess* GameProc;

	std::vector<NETWORK::Socket*> Clients;
	std::vector<NETWORK::Socket*> ClientsData;

	std::vector<GAME::GameEvent> EventsQueue[MaxPlayers];
	HANDLE EventQueueMutex[MaxPlayers];

	std::vector<ClientListener*> ClientListeners;
	ServerProcess* ServerProc;

	int numClients;
	std::string ClientNames[MaxPlayers];
	int GameRequests;
	int EndGameRequests;
	bool bRun;
	
	std::vector<GAME::Score> Scores;

public:
	Server() : GameProc(0) {}

	void AddEventToQueue(GAME::GameEvent ge, int client);
	void OnReceiveGameEventFromClient(int clientIndex, GAME::GameEvent gevent);
	void ProcessEvents();
	void LoadScores(const std::string& path);
	void SaveScores(const std::string& path);

	void Run(int players);
	void Release();

	void BroadcastEvent(GAME::GameEvent ge);
	void SynchronizeObjects();

	bool Running() const { return bRun; }

	void OnReceiveGameEvent(GAME::GameEvent gevent);
	void PrintLine(const std::string& msg);
};




class GameProcess : public BASE::Runnable
{
	friend Server;
private:
	GAME::Game* pGame;
	Server* pServer;
	//std::thread* pThread;
public:
	GameProcess(Server* s) : pServer(s) {}

	void Start(GAME::Game* game);
	void operator()();
	void Run() { operator()(); }
	void Release();
};

class ClientListener : public BASE::Runnable
{
private:
	int Index;
	Server* Serv;

	NETWORK::Socket* pClient;
	//std::thread* pThread;
public:
	ClientListener(int i, Server* serv) : Index(i), Serv(serv) {}

	void Join() { }//pThread->join(); }
	void Start(NETWORK::Socket* client);
	void Release();
	void operator()();
	void Run() { operator()(); }
};